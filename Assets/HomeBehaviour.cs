﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ActivateEndLevel()
    {
        StartCoroutine(EndLevelSequence());
    }

    private IEnumerator EndLevelSequence()
    {
        float speed = -8f;

        while(transform.position.y < 100)
        {

            transform.Translate(Vector3.up * speed * Time.deltaTime);

            speed += Time.deltaTime * 33f;

            yield return null;
             

        }

        UIController.instance.GoToNextLevel();
          
    }

}
