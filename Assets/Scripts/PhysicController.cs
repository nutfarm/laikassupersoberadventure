﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class PhysicController : MonoBehaviour
{

	public float _accelerationForce = 100f;
	public float _rotationForce = 100f;
    public float _maxVelocity;

    public float _startVelocityFromAsteroid;

    public KeyCode left;
    public KeyCode right;
    public KeyCode forward;

    public Rigidbody rigidbody;

    public VisualEffect assBlast;

    public GameObject explosion;

    private State _state = State.Flying;

    private GameObject _lockedAsteroid;

    public AudioClip CollectBeer;
    public AudioClip HomeEnter;
    public AudioClip Landing;

    public AudioClip ExplosionSound;

    public Animator animator;

    private enum Direction
    {
        None,
        Left,
        Right, 

    }

    private enum State
    {
        None,
        Flying,
        OnAsteroid,
        InHome,
        Dead,

    }

    private bool _accelerate = false;
    private float accelerationEffectPower = 0f;

    private Direction _direction = Direction.None;

    // Start is called before the first frame update
    void Start()
    {

        rigidbody = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {

        UpdateControls();
    }

    private void UpdateControls()
    {

        if (_state == State.Dead)
        {
            return;
        }

        if (_accelerate)
        {
            accelerationEffectPower += Time.deltaTime * 8f;            
        } else
        {
            accelerationEffectPower -= Time.deltaTime * 16f;
        }

        accelerationEffectPower = Mathf.Clamp(accelerationEffectPower, 0f, 4f);

        assBlast.SetFloat("New float", accelerationEffectPower);

        if (_state == State.InHome)
        {
            transform.position = _lockedAsteroid.transform.position;

            return;
        }

        if (Input.GetKeyDown(left))
        {
            _direction = Direction.Left;
        }

        if (Input.GetKeyDown(right))
        {
            _direction = Direction.Right;
        }

        if (Input.GetKeyUp(left) && _direction == Direction.Left)
        {
            _direction = Direction.None;
        }

        if (Input.GetKeyUp(right) && _direction == Direction.Right)
        {
            _direction = Direction.None;
        }

        if (Input.GetKeyDown(forward))
        {
            _accelerate = true;
            animator.SetBool("press", true);
            
        }

        if (Input.GetKeyUp(forward))
        {
            _accelerate = false;
            animator.SetBool("press", false);
        }


    }

    void FixedUpdate()
    {

        float dir = 0f;

        if (_direction == Direction.Left)
        {
            dir = -1f;
        }
        if (_direction == Direction.Right)
        {
            dir = 1f;
        }

        if (_state == State.Flying)
        {
            HandleFlying(dir);
        }

        if (_state == State.OnAsteroid)
        {
            HandleAsteroidControls(dir);
        }

    }

    private void HandleAsteroidControls(float dir)
    {
        if (_lockedAsteroid != null)
        {
            transform.position = _lockedAsteroid.transform.position + transform.forward * 2.5f;
                  
        }

        rigidbody.AddTorque(new Vector3(0, rigidbody.mass * _rotationForce * dir));

        //leave asteroid by accelerating
        if (_accelerate)
        {
            rigidbody.velocity = transform.forward * _startVelocityFromAsteroid;
            _state = State.Flying;

            //asteroid explodes when leaving it because it makes total sense

            Instantiate(explosion, _lockedAsteroid.transform.position, Quaternion.identity);

            Destroy(_lockedAsteroid);

            SoundManager.instance.PlaySingle(ExplosionSound);

            _lockedAsteroid = null;
            Camera.main.GetComponent<FollowPlayer>().isOnAsteroid = false;

        }

    }

    private void HandleFlying(float dir)
    {
        if (UIController.instance.GetFuel() < 0f)
        {
            return;
        }


        if (_accelerate)
        {
            rigidbody.AddForce(transform.forward * rigidbody.mass * _accelerationForce);

            UIController.instance.UseFuelAcceleration();
        }

        if (_direction != Direction.None)
        {
            UIController.instance.UseFuelRotate();
        }

        rigidbody.AddTorque(new Vector3(0, rigidbody.mass * _rotationForce * dir));

        //limit maximum velocity
        if (rigidbody.velocity.magnitude > _maxVelocity)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * _maxVelocity;
        }
    }

    void OnTriggerEnter(Collider col)
    {

        //land on asteroid
        if (col.tag == "Asteroid" && _state == State.Flying)
        {

            rigidbody.velocity = Vector3.zero;
            _accelerate = false;
            _direction = Direction.None;

            _lockedAsteroid = col.gameObject;
            _state = State.OnAsteroid;

            UIController.instance.ResetFuel();

            UIController.instance.AddBeer();

            Camera.main.GetComponent<FollowPlayer>().isOnAsteroid = true;

            //launch beer can off from asteroid
            col.gameObject.GetComponent<AsteroidBehaviour>().ReleaseBeer();

            SoundManager.instance.PlaySingle(CollectBeer);
            SoundManager.instance.PlaySingle(Landing);

        }

    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.tag == "Home" && _state == State.Flying && UIController.instance.HasEnoughBeer())
        {
            _state = State.InHome;
            rigidbody.velocity = Vector3.zero;
            _accelerate = false;

            _lockedAsteroid = collision.collider.gameObject;


            SoundManager.instance.PlaySingle(HomeEnter);

            collision.collider.GetComponentInParent<HomeBehaviour>().ActivateEndLevel();

            return;

        }

        if (_state == State.Flying)
        {
            Debug.Log(collision.collider.name);

            _state = State.Dead;
            _accelerate = false;
            StartCoroutine(Die());
        }

    }

    void OnParticleCollision(GameObject other)
    {
        if (_state == State.Flying)
        {

            _state = State.Dead;
            _accelerate = false;
            StartCoroutine(Die());
        }
    }

    private IEnumerator Die()
    {

        float n = 0;

        while (n < 0.5f)
        {
       
            n += Time.deltaTime;

            float min = 0.75f;
            float max = 1.25f;

            transform.localScale = new Vector3(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max));

            yield return null;
               
        }

        transform.localScale = Vector3.zero;
        rigidbody.velocity = Vector3.zero;

        SoundManager.instance.PlaySingle(ExplosionSound);

        //TODO tähän parit partikkelit

        Instantiate(explosion, transform.position, Quaternion.identity);

        yield return new WaitForSeconds(0.5f);

        UIController.instance.RestartLevel();

    }


}
