﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ParallaxScroller : MonoBehaviour
{
    public float ParallaxFactor = 0f;

    Transform theCamera;
    Vector3 theDimension;

    Vector3 theStartPosition;

    void Start() {
        theCamera = Camera.main.transform;
        theStartPosition = transform.position;

        theDimension = GetComponent<Renderer>().bounds.size;
    }

    void Update() {
        Vector3 newPos = theCamera.position * ParallaxFactor;                   // Calculate the position of the object
        newPos.y = -1;                       // Force Z-axis to zero, since we're in 2D
        newPos.x += theStartPosition.x;
        newPos.z += theStartPosition.z;
        transform.position = newPos;

        EndlessRepeater();
    }

    void EndlessRepeater() {

            if (theCamera.position.z > (transform.position.z + theDimension.z)) {
                theStartPosition.z += theDimension.z + theDimension.z;
            }
            if (theCamera.position.x > (transform.position.x + theDimension.x)) {
                theStartPosition.x += theDimension.x + theDimension.x;
            }
        
            if (theCamera.position.z < (transform.position.z - theDimension.z)) {
                theStartPosition.z -= theDimension.z + theDimension.z;
            }
            if (theCamera.position.x < (transform.position.x - theDimension.x)) {
                theStartPosition.x -= theDimension.x + theDimension.x;
            }
        
    
    }
}
