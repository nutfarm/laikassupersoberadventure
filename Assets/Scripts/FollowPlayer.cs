﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
 public float m_DampTime = 0.2f;                 // Approximate time for the camera to refocus.
    public float m_ScreenEdgeBuffer = 4f;           // Space between the top/bottom most target and the screen edge.
    public float m_MinSize = 6.5f;
    public float m_MaxSize = 15f;// The smallest orthographic size the camera can be.
    private Transform m_Targets; // All the targets the camera needs to encompass.


    private Camera m_Camera;                        // Used for referencing the camera.
    private float m_ZoomSpeed;                      // Reference speed for the smooth damping of the orthographic size.
    private Vector3 m_MoveVelocity;                 // Reference velocity for the smooth damping of the position.
    private Vector3 m_DesiredPosition;              // The position the camera is moving towards.

    public float offSetX;
    public float offSetY;
    public float offSetZ;

    private float rbVeloZ;
    private float rbVeloX;
    private float currentVelo;

    public float maxVelocity = 30f;

    [HideInInspector] public bool isOnAsteroid = false;
    public float asteroidZoomLevel = 15f;

    private PhysicController physicController;

    private void Awake ()
    {
        m_Camera = GetComponentInChildren<Camera> ();
    //    physicController = FindObjectOfType<PhysicController>();
     //   m_Targets = physicController.transform;
    }

    public void ResetCamera(Transform target)
    {
        physicController = FindObjectOfType<PhysicController>();
        m_Targets = target;
    }


    private void LateUpdate ()
    {
        if (m_Targets == null)
        {
            physicController = FindObjectOfType<PhysicController>();

            if (physicController == null)
            {
                return;
            }

            m_Targets = physicController.transform;
            return;
        }

        // Move the camera towards a desired position.
        Move ();

        // Change the size of the camera based.
        Zoom ();
    }


    private void Move ()
    {
        // Find the average position of the targets.
        FindAveragePosition ();

        // Smoothly transition to that position.
        transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }


    private void FindAveragePosition ()
    {
        Vector3 averagePos = new Vector3 ();

        // Add to the average and increment the number of targets in the average.
        averagePos = m_Targets.position;


        // Keep the same y value.
        averagePos.y = averagePos.y + offSetY;
        averagePos.x = averagePos.x + offSetX;
        averagePos.z = averagePos.z + offSetZ;
        // The desired position is the average position;
        m_DesiredPosition = averagePos;
    }


    private void Zoom ()
    {
        // Find the required size based on the desired position and smoothly transition to that size.
        float requiredSize = FindRequiredSize();
   //     rbVeloX = physicController.rigidbody.velocity.x;
     //   rbVeloZ = physicController.rigidbody.velocity.z;
      //  currentVelo = Mathf.Max(Mathf.Max(Mathf.Abs(rbVeloX), Mathf.Abs(rbVeloZ)),1f);

        float velocity = physicController.gameObject.GetComponent<Rigidbody>().velocity.magnitude;


        float t = Mathf.InverseLerp(0, maxVelocity, velocity); // returns a value between 0-1.
        t = Mathf.SmoothStep(0, 1, t); // smooth out the t value so that the camera will ease in and out nicely.
        requiredSize = Mathf.Lerp(m_MinSize, m_MaxSize, t); // blend between these two points

        if (isOnAsteroid)
        {
            requiredSize = asteroidZoomLevel;
        }
        
        m_Camera.orthographicSize = Mathf.SmoothDamp (m_Camera.orthographicSize, requiredSize, ref m_ZoomSpeed, m_DampTime);
    }


    private float FindRequiredSize ()
    {
        // Find the position the camera rig is moving towards in its local space.
        Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        // Start the camera's size calculation at zero.
        float size = 0f;



            // Otherwise, find the position of the target in the camera's local space.
            Vector3 targetLocalPos = transform.InverseTransformPoint(m_Targets.position);

            // Find the position of the target from the desired position of the camera's local space.
            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            // Choose the largest out of the current size and the distance of the tank 'up' or 'down' from the camera.
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));

            // Choose the largest out of the current size and the calculated size based on the tank being to the left or right of the camera.
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / m_Camera.aspect);
        

        // Add the edge buffer to the size.
        size += m_ScreenEdgeBuffer;

        // Make sure the camera's size isn't below the minimum.
        size = Mathf.Max (size, m_MinSize);

        return size;
    }


    public void SetStartPositionAndSize ()
    {
        // Find the desired position.
        FindAveragePosition ();

        // Set the camera's position to the desired position without damping.
        transform.position = m_DesiredPosition;

        // Find and set the required size of the camera.
        m_Camera.orthographicSize = FindRequiredSize ();
    }
}
