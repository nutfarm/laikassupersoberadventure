﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public static UIController instance;

    public RectTransform fuelBar;
    public float fuelBarLength = 50;

    public float maxFuel = 100f;

    public float rotationConsumption = 10f;
    public float accelerationConsumption = 20f;

    public int currentLevel = 0;

    public List<GameObject> levelPrefabs = new List<GameObject>();

    private float _currentFuel = 100f;

    private Vector3 _fuelBarStartLocation;

    public GameObject _currentLevel;

    public Text _beerCounter;

    private int requiredBeer = 5;
    private int currentBeer = 5;

    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        _fuelBarStartLocation = fuelBar.anchoredPosition;
        _currentFuel = maxFuel;

        StartLevel();
    }

    public void GoToNextLevel()
    {
        currentLevel++;
        StartLevel();
    }

    public void RestartLevel()
    {

        StartLevel();
    }

    private void StartLevel()
    {

        //destroy old level and load new one on it
        Destroy(_currentLevel);

        _currentLevel = (GameObject)Instantiate(levelPrefabs[currentLevel], Vector3.zero, Quaternion.identity);

        currentBeer = 0;
        requiredBeer = _currentLevel.GetComponent<LevelData>().requiredBeer;

        //create player
        GameObject player = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        player.transform.parent = _currentLevel.transform;

        Camera.main.GetComponent<FollowPlayer>().ResetCamera(player.transform);

        ResetFuel();

        UpdateBeerCounter();
    }

    public void AddBeer()
    {
        currentBeer++;
        UpdateBeerCounter();
    }

    private void UpdateBeerCounter()
    {
        _beerCounter.text = currentBeer + " / " + requiredBeer;
    }

    public float GetFuel()
    {
        return _currentFuel;
    }

    public void UseFuelRotate()
    {
        _currentFuel -= Time.deltaTime * rotationConsumption;

        UpdateFuel();
    }

    public void UseFuelAcceleration()
    {
        _currentFuel -= Time.deltaTime * accelerationConsumption;

        UpdateFuel();
    }

    private void UpdateFuel()
    {
        //  fuelBar.transform.position = fuelBarStartLocation;
        //   fuelBar.transform.position += -Vector3.up * currentFuel * fuelBarLength / maxFuel;

        fuelBar.anchoredPosition = _fuelBarStartLocation + -Vector3.up * (maxFuel - _currentFuel) * fuelBarLength / maxFuel;

    }

    public void ResetFuel()
    {
        _currentFuel = maxFuel;
        UpdateFuel();
    }

    public bool HasEnoughBeer()
    {
        return currentBeer >= requiredBeer;
    }

}
