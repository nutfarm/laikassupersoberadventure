﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotaattori2 : MonoBehaviour
{

    public Vector3 rotation;

    void Start()
    {
        rotation = new Vector3(Random.Range(-rotation.x, rotation.x), Random.Range(-rotation.y, rotation.y), Random.Range(-rotation.z, rotation.z));
    }

    void Update()
    {
        transform.Rotate(rotation * Time.deltaTime);

    }
}

