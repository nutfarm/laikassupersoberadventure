﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scenechanger : MonoBehaviour
{
    public float time = 20f;
    public int sceneNumber = 1;


    // Start is called before the first frame update
    void Start()
    {
        Invoke("Change", time);
    }

    void Change()
    {
        SceneManager.LoadScene(sceneNumber);
    }
}
