﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerCanGravity : MonoBehaviour
{


    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<Rigidbody>().AddForce(Physics.gravity * 2);   
    }
}
