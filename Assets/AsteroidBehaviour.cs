﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : MonoBehaviour
{

    public Transform beerCan;

    public void ReleaseBeer()
    {
        //    StartCoroutine(BeerLaunch());

        Rigidbody rigid = beerCan.GetComponent<Rigidbody>();

        rigid.isKinematic = false;
        rigid.AddForce(new Vector3(Random.Range(-200, 200), Random.Range(222, 333), Random.Range(-200, 200)));
        rigid.AddTorque(new Vector3(Random.Range(-200, 200), Random.Range(-200, 200), Random.Range(-200, 200)));

        beerCan.parent = UIController.instance._currentLevel.transform;

    }


}
